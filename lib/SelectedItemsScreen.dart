import 'package:flutter/material.dart';
import 'FoodMenu.dart';
import 'SelectedFood.dart';

class SelectedItemsScreen extends StatefulWidget {
  final Map<String, SelectedFood> selectedItems;
  final Map<String, FoodMenu> foodMenu; // Change this to a Map

  const SelectedItemsScreen({
    Key? key,
    required this.selectedItems,
    required this.foodMenu, // Update the type here
  }) : super(key: key);

  @override
  _SelectedItemsScreenState createState() => _SelectedItemsScreenState();
}

class _SelectedItemsScreenState extends State<SelectedItemsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'รายการอาหารที่เลือก',
          style: TextStyle(fontSize: 30, color: Colors.black),
        ),
      ),
      body: ListView.builder(
        itemCount: widget.selectedItems.length,
        itemBuilder: (BuildContext context, int index) {
          String key = widget.selectedItems.keys.elementAt(index);
          SelectedFood food = widget.selectedItems[key]!;
          FoodMenu menu = widget.foodMenu[key]!; // Retrieve the menu item for the selected food
          return ListTile(
            leading: Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                  image: AssetImage(food.imageFood),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            title: Text(
              '${food.name} x${food.quantity}',
              style: const TextStyle(fontSize: 30, color: Colors.black),
            ),
            subtitle: Text(
              "ราคารวม: ${menu.price * food.quantity} Bux", // Calculate the total price using menu price
              style: const TextStyle(fontSize: 20, color: Colors.black54),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      if (food.quantity > 1) {
                        food.quantity--;
                      } else {
                        widget.selectedItems.remove(food.name);
                      }
                    });
                  }, // Set icon color to black
                  style: ElevatedButton.styleFrom(
                    shape: const CircleBorder(), backgroundColor: Colors.red, // Change the color of the button
                  ),
                  child: const Icon(Icons.remove, color: Colors.black),
                ),
                Text(
                  food.quantity.toString(),
                  style: const TextStyle(fontSize: 24, color: Colors.black),
                ),
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      food.quantity++;
                    });
                  }, // Set icon color to black
                  style: ElevatedButton.styleFrom(
                    shape: const CircleBorder(), backgroundColor: Colors.green, // Change the color of the button
                  ),
                  child: const Icon(Icons.add, color: Colors.black),
                ),
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            widget.selectedItems.clear();
          });
          Navigator.pop(context); // Navigate back to the main screen
        },
        child: const Icon(Icons.refresh),
      ),
    );
  }
}
