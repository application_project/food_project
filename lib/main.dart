import 'package:flutter/material.dart';
import 'SelectedItemsScreen.dart';
import 'FoodMenu.dart';
import 'SelectedFood.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My App',
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.purple,
          brightness: Brightness.dark,
        ).copyWith(
          surface: Colors.orange[100], // Light orange background
          primary: Colors.orange, // Set the primary color to orange
        ),
        scaffoldBackgroundColor: Colors.orange[100], // Light orange background
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.orange, // Set AppBar background color
        ),
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // Group Data
  final Map<String, FoodMenu> menu = {
    "กะเพราหมู": FoodMenu("กะเพราหมู", 50, "assets/images/กะเพราหมู.jpg"),
    "กะเพราไก่": FoodMenu("กะเพราไก่", 50, "assets/images/กะเพราไก่.jpg"),
    "กะเพราเนื้อ": FoodMenu("กะเพราเนื้อ", 60, "assets/images/กะเพราเนื้อ.jpg"),
  };

  // Selected Items
  final Map<String, SelectedFood> selectedItems = {};

  // Show Data
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'เลือกเมนูอาหารได้แล้ว ไอเวร',
          style: TextStyle(
              fontSize: 40,
              color: Colors.black), // Adjusted font size for better readability
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.shopping_cart),
            iconSize: 30,
            color: Colors.black, // Set icon color to black for better visibility
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => SelectedItemsScreen(selectedItems: selectedItems, foodMenu: menu),
                ),
              );
            },
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: menu.length, // show n data
        itemBuilder: (BuildContext context, int index) {
          String key = menu.keys.elementAt(index);
          FoodMenu food = menu[key]!;
          return ListTile(
            leading: Container(
              width: 60, // Set the desired width
              height: 60, // Set the desired height
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                    8), // Optional: Add some border radius
                image: DecorationImage(
                  image: AssetImage(food.imageFood),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            title: Text(
              food.name,
              style: const TextStyle(
                  fontSize: 30,
                  color: Colors.black), // Adjusted font size for better readability
            ),
            subtitle: Text(
              "ราคา ${food.price} Bux", // Correctly concatenating the price
              style: const TextStyle(
                  fontSize: 20, color: Colors.black54), // Styling the subtitle
            ),
            trailing: IconButton(
              icon: const Icon(Icons.add, size: 30),
              color: Colors.black, // Set icon color to black for better visibility
              onPressed: () {
                setState(() {
                  if (selectedItems.containsKey(food.name)) {
                    selectedItems[food.name]!.quantity += 1;
                    selectedItems[food.name]!.totalPrice += food.price;
                  } else {
                    selectedItems[food.name] = SelectedFood(food.name, 1, food.price, food.imageFood);
                  }
                });
              },
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SelectedItemsScreen(selectedItems: selectedItems, foodMenu: menu),
            ),
          );
        },
        child: const Icon(Icons.shopping_cart),
      ),
    );
  }
}
