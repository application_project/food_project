class SelectedFood {
  String name;
  int quantity;
  int totalPrice;
  String imageFood;

  SelectedFood(this.name, this.quantity, this.totalPrice, this.imageFood);
}
